;;;; Snippets.el -- Small snippets of code

(defun my/digit-argument-or-run-function (num func)
  "If NUM is equal to NIL, run FUNC. Run `(digit-argument NUM)' otherwise"
  (if (eql num nil)
      (funcall func)
      (digit-argument num)))
