;;;; My Emacs configuration

;;; Basic Setup

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(when (< emacs-major-version 29)
  (require 'use-package))

(setq custom-file (concat user-emacs-directory "emacs-custom.el"))
(when (file-exists-p custom-file)
  (load-file custom-file))

;; https://emacs-lsp.github.io/lsp-mode/page/performance/
(setq gc-cons-threshold 100000000
      read-process-output-max (* 1024 1024))

;; https://www.masteringemacs.org/article/speed-up-emacs-libjansson-native-elisp-compilation
(if (and (fboundp 'native-comp-available-p)
         (native-comp-available-p))
    (setq comp-deferred-compilation t
          package-native-compile t)
  (message "Native complation is *not* available, lsp performance will suffer..."))

(unless (functionp 'json-serialize)
  (message "Native JSON is *not* available, lsp performance will suffer..."))

;; post-init config directory
(defvar my/post-init-lib-dir (concat user-emacs-directory "lib-post-init/")
  "Directory containing files to load after init and before files in `post-init-config-dir' are loaded")

(unless (file-directory-p my/post-init-lib-dir)
  (make-directory my/post-init-lib-dir))

(when (file-directory-p my/post-init-lib-dir)
  (dolist (file (directory-files my/post-init-lib-dir nil "^.*\.el"))
    (load-file (concat my/post-init-lib-dir file))))

;;; Uncatagorized Settings

;; utf-8
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

;; Unix line ending
(setq-default buffer-file-coding-system 'prefer-utf-8-unix)

;; clean UI
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Editing
(global-auto-revert-mode 1)
(electric-pair-mode 1)
(delete-selection-mode)
(setq-default indent-tabs-mode nil
              tab-width 4
              fill-column 80)

;; bell
(setq ring-bell-function 'ignore)

;; startup
(setq inhibit-startup-screen t)
(savehist-mode 1)
(recentf-mode 1)

;; M-x
(setq read-extended-command-predicate #'command-completion-default-include-p)

;; nicer scrolling
(when (>=  emacs-major-version 29)
  (pixel-scroll-precision-mode 1))
(setq scroll-conservatively 1000)
(setq scroll-margin 3)

;; add column number
(column-number-mode)

;; default browser
(setq browse-url-browser-function 'eww-browse-url
      browse-url-generic-program "firefox")

;; ibuffers everywhere
(defalias 'list-buffers #'ibuffer)

;; keys
(global-set-key (kbd "C-x SPC") #'set-mark-command)
(global-set-key (kbd "C-x /") #'comment-line)
(global-set-key (kbd "C-x ?") #'comment-or-uncomment-region)
(global-set-key (kbd "C-x 4 C-b") #'ibuffer-other-window)
(global-set-key (kbd "C-z") #'repeat)

;;; Colorscheme

(defvar my/light-theme 'tango)
(defvar my/dark-theme 'tango-dark)

(defun my/boxed-hl-line-face ()
  (when (and (facep 'hl-line) (facep 'highlight))
    (let ((default-bg (face-attribute 'default :background))
          (hl-line-bg (if (eql (face-attribute 'hl-line :background) 'unspecified)
                          (face-attribute 'highlight :background)
                        (face-attribute 'hl-line :background))))
      (unless (eql default-bg hl-line-bg)
        (set-face-attribute 'hl-line nil
                            :foreground 'unspecified
                            :background default-bg
                            :extend t
                            :box (list :line-width '(-1 . -2)
                                       :color hl-line-bg
                                       :style nil)
                            :inherit nil)))))

(defun my/gnome-shell-uses-light-theme-p ()
  (and
     (executable-find "gsettings")
     (equal (shell-command-to-string "gsettings get org.gnome.desktop.interface color-scheme") "'default'\n")))

(use-package solarized-theme
  :ensure t
  :config
  (setq solarized-use-variable-pitch nil
        solarized-use-more-italic t
        solarized-height-plus-1 1.0
        solarized-height-plus-2 1.0
        solarized-height-plus-3 1.0
        solarized-height-plus-4 1.0))

(use-package timu-caribbean-theme 
  :ensure t)

(setq my/light-theme 'solarized-light
      my/dark-theme 'solarized-dark)

(if (and (my/gnome-shell-uses-light-theme-p))
    (load-theme my/light-theme t)
  (load-theme my/dark-theme t))

;;; Face Font

(when (fontp (font-spec :family "Sarasa Mono TC"))
  (set-face-attribute 'default nil
                      :family "Sarasa Mono TC"
                      :height 148)  ; sizes that works: 132..139 147..154
  (set-face-attribute 'fixed-pitch nil
                      :family "Sarasa Mono TC"))

(when (fontp (font-spec :family "Sarasa Mono Slab TC"))
  (set-face-attribute 'fixed-pitch-serif nil
                      :family "Sarasa Mono Slab TC"))

;;; which-key

(use-package which-key
  :ensure t
  :custom
  (which-idle-delay 0.2)
  :init
  (which-key-mode))

;;; windmove and windows

(defun my/prefix-or-delete-window (&optional num)
  (interactive "P")
  (my/digit-argument-or-run-function num #'delete-window))
(define-key global-map (kbd "C-0") #'my/prefix-or-delete-window)

(use-package windmove
  :config
  (global-unset-key (kbd "C-x o"))
  (define-key global-map (kbd "C-x o o") #'other-window)
  (define-key global-map (kbd "C-x o O") #'window-swap-states)
  (define-key global-map (kbd "C-x O") #'window-swap-states)
  (define-key windmove-mode-map (kbd "C-x o n") #'windmove-down)
  (define-key windmove-mode-map (kbd "C-x o N") #'windmove-swap-states-down)
  (define-key windmove-mode-map (kbd "C-x o p") #'windmove-up)
  (define-key windmove-mode-map (kbd "C-x o P") #'windmove-swap-states-up)
  (define-key windmove-mode-map (kbd "C-x o f") #'windmove-right)
  (define-key windmove-mode-map (kbd "C-x o F") #'windmove-swap-states-right)
  (define-key windmove-mode-map (kbd "C-x o b") #'windmove-left)
  (define-key windmove-mode-map (kbd "C-x o B") #'windmove-swap-states-left)
  (windmove-mode))

;;; 中文體驗

(use-package kinsoku
  :config
  (setq word-wrap-by-category t))

(use-package sis
  :ensure t
  :config
  (sis-ism-lazyman-config "xkb:us::eng" "rime" 'ibus)
  (sis-global-respect-mode 1))

;; prog-mode and text-mode hook

(defun my/prog-mode-hook ()
  (toggle-truncate-lines 1)
  (display-line-numbers-mode 1)
  (hl-line-mode))
(add-hook 'prog-mode-hook #'my/prog-mode-hook)

(defun my/text-mode-hook ()
  (visual-line-mode 1))
(add-hook 'text-mode-hook #'my/text-mode-hook)

;;; orderless

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

;;; minibuffer

(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  :config
  (setq vertico-scroll-margin 1)
  (setq vertico-resize t))
(use-package vertico-directory
  :after vertico
  :config
  (define-key vertico-map (kbd "M-DEL") #'vertico-directory-delete-word))

(setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
(setq enable-recursive-minibuffers t)

;;; completion and snippets

(setq tab-always-indent 'complete)

(use-package corfu
  :ensure t
  :init
  (global-corfu-mode))

(use-package cape
  :ensure t
  :init
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-abbrev)
  (add-hook 'completion-at-point-functions #'cape-dabbrev))

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))

;;; LSP and Eldoc

(defun my/eldoc-visual-line (&rest r)
  (with-current-buffer eldoc--doc-buffer
    (turn-on-visual-line-mode)))
(advice-add 'eldoc-doc-buffer :after #'my/eldoc-visual-line)

(global-eldoc-mode 1)
(setq eldoc-echo-area-display-truncation-message nil
      eldoc-echo-area-use-multiline-p nil)

(use-package eldoc-box
  :ensure t)

(use-package eglot
  :ensure t
  :config
  (define-key eglot-mode-map (kbd "C-c r") #'eglot-rename)
  (when (and (<= 28 emacs-major-version) (package-installed-p 'eldoc-box))
    (define-key eglot-mode-map (kbd "C-c .") #'eldoc-box-help-at-point)))

;; (use-package eglot-x
;;   :after eglot
;;   :config
;;   (eglot-x-setup)
;;   (define-key eglot-mode-map (kbd "C-c M-.") #'eglot-x-find-refs))

(use-package flymake
  :config
  (define-key flymake-mode-map (kbd "C-c l") #'flymake-show-buffer-diagnostics)
  (define-key flymake-mode-map (kbd "C-c C-l") #'flymake-show-buffer-diagnostics))

;;; darkroom-mode

(use-package darkroom
  :ensure t
  :config
  (setq darkroom-text-scale-increase 0))

;;; Terminal app - Eat

(use-package eat
  :ensure t
  :hook
  (eshell-load-hook . #'eat-eshell-mode))

;;; Small web browsing - Elpher

(defun my/elpher-mode-hook ()
  (darkroom-tentative-mode)
  (setq-local scroll-margin 0))

(use-package elpher
  :ensure t
  :config
  (setq elpher-gemini-bullet-string "⁃")
  (add-hook 'elpher-mode-hook #'my/elpher-mode-hook)
  (define-key elpher-mode-map (kbd "+") #'darkroom-increase-margins)
  (define-key elpher-mode-map (kbd "_") #'darkroom-decrease-margins)
  (define-key elpher-mode-map (kbd "p") #'previous-line)
  (define-key elpher-mode-map (kbd "n") #'next-line))

;;; AI - Ellama

(use-package ellama
  :ensure t)

;;; Git - Magit

(use-package magit
  :ensure t)

;;; lang-c

(when (executable-find "clangd")
  (add-to-list 'eglot-server-programs '((c++-mode c-mode) "clangd")))
(add-hook 'c-mode-hook 'eglot-ensure)
(add-hook 'c++-mode-hook 'eglot-ensure)

(add-hook 'c-mode-hook (lambda ()
                         (setq tab-width 8)
                         (c-set-style "linux")))
(add-hook 'c++-mode-hook (lambda ()
                         (setq tab-width 8)
                         (c-set-style "linux")))

;;; lang-lisp

(use-package sly
  :ensure t
  :config
  (when (executable-find "sbcl")
      (setq inferior-lisp-program "sbcl")))

(use-package paredit
  :ensure t
  :config
  (add-hook 'lisp-mode #'paredit-mode))

;;; lang-lua

(use-package lua-mode
  :ensure t
  :after eglot
  :config
  (when (executable-find "lua-language-server")
    (add-to-list 'eglot-server-programs
             `(lua-mode . ("lua-language-server")))
    (add-hook 'lua-mode-hook #'eglot-ensure))
  (add-hook 'lua-mode-hook (lambda ()
                             (setq tab-width 3)
                             (setq lua-indent-level 3))))

;;; lang-rust

(use-package rust-mode
  :ensure t
  :after eglot
  :config
  (when (executable-find "rust-analyzer")
    (require 'eglot)
    (add-hook 'rust-mode-hook #'eglot-ensure)
    (add-to-list 'eglot-server-programs
             '((rust-ts-mode rust-mode) .
               ("rust-analyzer" :initializationOptions (:check (:command "clippy")))))))

;;; lang-org

(use-package org
  :config
  (defun my/org-mode-hook ()
    (org-indent-mode 1))
  (add-hook 'org-mode-hook #'my/org-mode-hook))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/notes/org"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

;;; lang-md

(use-package markdown-mode
  :ensure t
  :config
  (define-key markdown-mode-map (kbd "C-c RET") #'markdown-follow-thing-at-point)
  (when (executable-find "pandoc")
    (setq markdown-command "pandoc -f markdown_mmd")))

